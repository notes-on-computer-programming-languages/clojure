# clojure

Lisp dialect for the JVM. https://en.m.wikipedia.org/wiki/Clojure

![popcon](https://qa.debian.org/cgi-bin/popcon-png?packages=rustc%20ocaml-base-nox%20ghc%20julia%20scala%20racket%20elixir%20clojure%20chezscheme&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%Y&beenhere=1)

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=racket+clojure+chezscheme+newlisp&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%Y)

# Books
* [Clojure (Computer program language)
  ](https://www.worldcat.org/search?q=su%3AClojure+%28Computer+program+language%29)
---
* [*Functional Design: Principles, Patterns, and Practices*
  ](https://www.oreilly.com/library/view/functional-design-principles/9780138176518/)
  2023-10 Robert C. Martin (Addison-Wesley Professional)
* *Web Development with Clojure* 2021 (3rd Edition)
* *Programming Clojure*
  2018 (Third Edition) Alex Miller
* [*Functional Programming: A PragPub Anthology: Exploring Clojure, Elixir, Haskell, Scala, and Swift*
  ](https://pragprog.com/titles/ppanth/functional-programming-a-pragpub-anthology/)
  2017-07 Michael Swaine and the PragPub writers

# Libraries
## Standard library
Clojure standard library has give rise to libraries in other languages. [*](https://pypi.org/project/toolz/)
* Javascript: Underscore.js, Lodash [**](https://en.wikipedia.org/wiki/Underscore.js),
* PHP: various Underscore or Lodash related libraries (see Packagist),
* Python: toolz,
* Ruby: Enumerable (how does it compare with Elixir?).

## SQL
### About
* [*Clojure SQL library showdown: I heard you like DSLs* 
  ](https://adambard.com/blog/clojure-sql-libs-compared/)
  2015-12 Adam Bard

### List
* ![ClojarsDownloads](https://img.shields.io/clojars/dt/com.layerware/hugsql)
  [HugSQL](https://clojars.org/com.layerware/hugsql)

## Web
### Libraries
* ![ClojarsDownloads](https://img.shields.io/clojars/dt/ring)
  [ring](https://clojars.org/ring)
### Frameworks
* [clojurescript nodejs framework](https://www.google.com/search?q=clojurescript+nodejs+framework)
* [clojure web framework](https://www.google.com/search?q=clojure+web+framework)
---
* [Macchiato](https://macchiato-framework.github.io) is a ClojureScript micro-framework [clojars.org](https://clojars.org/search?q=macchiato)
  * ![Macchiato Downloads](https://img.shields.io/clojars/dt/macchiato/core)
    [macchiato/core](https://clojars.org/macchiato/core)
---
* [*Learn to build a Clojure web app - a step-by-step tutorial*
  ](https://ericnormand.me/guide/clojure-web-tutorial)
  2023-02 Eric Normand
* [*7 Top Free and Open Source Clojure Web Frameworks*
  ](https://www.linuxlinks.com/free-open-source-clojure-web-frameworks/)
  2022-08 Eilidih Parris
* [*What Web Framework Should I Use in Clojure?*
  ](https://ericnormand.me/mini-guide/what-web-framework-should-i-use-in-clojure)
  2022-06 Eric Normand
* [*Clojure Web Servers*
  ](https://ericnormand.me/mini-guide/clojure-web-servers)
  2021-03 Eric Normand
* [*Clojure like it's PHP: Use Clojure on Shared Web Hosts*
  ](https://eccentric-j.com/blog/clojure-like-its-php.html)
  2021-02 Eccentric J

# Implementations
* [Other_Implementations](https://en.wikipedia.org/wiki/Clojure#Other_Implementations) @ Wikipedia
  * ![Babashka Downloads](https://img.shields.io/clojars/dt/babashka)
  [babashka](https://clojars.org/babashka)
  Native, fast starting Clojure interpreter for scripting
  * [*Clojure like it's PHP: Use Clojure on Shared Web Hosts*
    ](https://eccentric-j.com/blog/clojure-like-its-php.html)
    2021-02 Eccentric J
---
* ![ClojarsDownloads](https://img.shields.io/clojars/dt/org.babashka/sci)
  [sci](https://clojars.org/org.babashka/sci)
  Configurable Clojure interpreter suitable for scripting and Clojure DSLs

## ClojErl
* [*BEAM all the things! ClojErl, an implementation of Clojure on the Erlang Virtual Machine*
  ](https://www.notamonadtutorial.com/clojerl-an-implementation-of-the-clojure-language-that-runs-on-the-beam/)
2021-07 Not a Monad Tutorial

# Some packages by popularity
* ![Ring Downloads](https://img.shields.io/clojars/dt/ring)
  [ring](https://clojars.org/ring)
* ![HugSQL Downloads](https://img.shields.io/clojars/dt/com.layerware/hugsql)
  [HugSQL](https://clojars.org/com.layerware/hugsql)
* ![SCI Downloads](https://img.shields.io/clojars/dt/org.babashka/sci)
  [sci](https://clojars.org/org.babashka/sci)
  Configurable Clojure interpreter suitable for scripting and Clojure DSLs
* ![Macchiato Downloads](https://img.shields.io/clojars/dt/macchiato/core)
  [macchiato/core](https://clojars.org/macchiato/core)
* ![Babashka Downloads](https://img.shields.io/clojars/dt/babashka)
  [babashka](https://clojars.org/babashka)
  Native, fast starting Clojure interpreter for scripting

